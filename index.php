<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aula 14</title>
</head>
<body>
    <h1>Aula 14 - PHP OO </h1>
    <hr>
    <h3>Alunos: Geovana Oliveira Gomes - Turma: AUT2D1</h3>
    <hr>
    <?php
        require_once 'usuario_class.php';
        $usuario1 = new Usuario('José da Silva','111.111.111-11','Rua das camélias, 25');

        echo $usuario1->nome;
        echo '<br>';
        echo $usuario1->getCpf();
        echo '<br>';
        echo $usuario1->getEndereco();
        echo '<br>';

        echo '<hr>';

        $usuario2 = new Usuario('Maria da Silva','222.222.222-22','Rua das camélias, 25');

        echo $usuario2->nome;
        echo '<br>';
        echo $usuario2->getCpf();
        echo '<br>';
        echo $usuario2->getEndereco();
        echo '<br>';

        echo '<hr>';

        $usuario3 = new Usuario('Ana da Silva','333.333.333-33','Rua das camélias, 25');

        echo $usuario3->nome;
        echo '<br>';
        echo $usuario3->getCpf();
        echo '<br>';
        echo $usuario3->getEndereco();
        echo '<br>';

        echo '<hr>';

        $usuario4 = new Usuario('João da Silva','444.444.444-44','Rua das camélias, 25');

        echo $usuario4->nome;
        echo '<br>';
        echo $usuario4->getCpf();
        echo '<br>';
        echo $usuario4->getEndereco();
        echo '<br>';
    ?>
</body>
</html>
